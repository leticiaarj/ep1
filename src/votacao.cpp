#include "votacao.hpp"
#include<iostream>
#include<fstream>
#include"politico.hpp"

using namespace std;

void Votacao::ler_arquivo_BR(){
	 
	string dados_inrelevantes;
	
	//ofstream relatorio_votacao_br;
        //relatorio_votacao_br.open("Relatorio_votacao_br", ios::app);

	ifstream dados_br;
	dados_br.open("data/consulta_cand_2018_BR.csv");
    	
	if(dados_br.is_open()){
		for(int z=0; z<27; z++){			
			getline(dados_br, this->nm_ue, ',');
			
			getline(dados_br, this->ds_cargo, ',');

			getline(dados_br, this->nr_candidato, ',');

			getline(dados_br, this->nm_urna_candidato, ',');

			getline(dados_br, this->nm_partido, '\n');

			Politico candidato = Politico(nm_ue, ds_cargo, nr_candidato, nm_urna_candidato, nm_partido);
	
			candidatos.push_back(candidato);// Jogando no array de candidatos
		}
	}else{
		cout << "Não foi possível abrir o arquivo" << endl;		
    	}
	dados_br.close();
   }


 void Votacao::ler_arquivo_DF(){
	
	string dados_inrelevantes;
	
	ifstream dados_df;
       	dados_df.open("data/consulta_cand_2018_DF.csv");
       
       	if(dados_df.is_open()){
                for(int z = 0; z<1238; z++){
			getline(dados_df, this->nm_ue, ',');
				
			getline(dados_df, this->ds_cargo, ',');

			getline(dados_df, this->nr_candidato, ',');

			getline(dados_df, this->nm_urna_candidato, ',');

			getline(dados_df, this->nm_partido, '\n');
			
			Politico candidato = Politico(nm_ue, ds_cargo, nr_candidato, nm_urna_candidato, nm_partido);
	

                        candidatos.push_back(candidato);
		}
                
        }else{
                cout << "Não foi possível abrir o arquivo" << endl;
        }
        dados_df.close();
  }

void Votacao::achar_candidato(string numero_candidato, string cargo){
	for(int i = 0; i<candidatos.size(); i++){
		if(candidatos[i].nr_candidato == numero_candidato && candidatos[i].ds_cargo == cargo){
		
		cout << "Unidade Federativa: " <<candidatos[i].nm_ue << endl;
                cout << "Cargo: " <<candidatos[i].ds_cargo << endl;
                cout << "Número do Candidato: " << candidatos[i].nr_candidato << endl;
                cout << "Nome: " << candidatos[i].nm_urna_candidato << endl;
                cout << "Número Partido: " << candidatos[i].nm_partido << endl;

		
		}
	}
}
