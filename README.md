# Exercício de Programação 1

#Diretório
Acesse no diretório: https://gitlab.com/leticiaarj/ep1.git

## Como usar o projeto

* Compile o projeto com o comando:

make

* Insira a quantidade de eleitores que irão votar;
* Insira o nome do eleitor a cada passo de votação;
* Responda sobre o voto em branco;
* Digite os números dos candidatos, respectivamente:

Presidente
Deputado Federal
Deputado Distrital
Governador 
Senador

## Funcionalidades do projeto
* Simula votações como em uma urna eletrônica;
* Exibe os dados de cada um dos candidatos;
* Tem a alternativa de votar em branco;
* Tem a alternativa de cancelar.

## Bugs e problemas

## Referências

* http://www.cplusplus.com/
* https://pt.wikipedia.org/wiki/C%2B%2B
