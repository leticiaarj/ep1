#ifndef POLITICO_HPP
#define POLITICO_HPP

#include <iostream>
#include <string>


using namespace std;

class Politico{

public:
	string nm_ue;
	string ds_cargo;
	string nr_candidato;
	string nm_urna_candidato;
	string nm_partido;

public:
	Politico(string, string, string, string, string);

	string get_nm_ue();
	void set_nm_ue(string nm_ue);


	string get_ds_cargo();
        void set_ds_cargo(string ds_cargo);


	string get_nr_candidato();
	void set_nr_candidato(string nr_candidato);


	string get_nm_urna_candidato();
        void set_nm_urna_candidato(string nm_urna_candidato);


	string get_nm_partido();
	void set_nm_partido(string nm_partido);

};

#endif
