#ifndef VOTACAO_HPP
#define VOTACAO_HPP

#include <iostream>
#include <string>
#include<vector>
#include"politico.hpp"
using namespace std;

class Votacao{
	public:
		string nm_ue;
		string ds_cargo;
        	string nr_candidato;
        	string nm_urna_candidato;
        	string nm_partido;

		vector<Politico> candidatos;
	     	void ler_arquivo_DF();
		void ler_arquivo_BR();	
		void achar_candidato(string, string);
};

#endif
