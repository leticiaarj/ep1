#include "politico.hpp"
#include<iostream>


Politico::Politico(string nm_ue, string ds_cargo, string nr_candidato, string nm_urna_candidato, string nm_partido){
	set_nm_ue(nm_ue);
	set_ds_cargo(ds_cargo);
	set_nr_candidato(nr_candidato);
	set_nm_urna_candidato(nm_urna_candidato);
	set_nm_partido(nm_partido);   
}	
	string Politico:: get_nm_ue(){
		return nm_ue;
	}
	void Politico:: set_nm_ue(string sg_ue){
		this->nm_ue = nm_ue;
	}


	string Politico:: get_ds_cargo(){
		return ds_cargo;
	}
        void Politico:: set_ds_cargo(string ds_cargo){
		this->ds_cargo = ds_cargo;
	}


	string Politico:: get_nr_candidato(){
		return nr_candidato;
	}
	void Politico:: set_nr_candidato(string nr_candidato){
		this->nr_candidato = nr_candidato;
	}


	string Politico:: get_nm_urna_candidato(){
		return nm_urna_candidato;
	}
        void Politico:: set_nm_urna_candidato(string nm_urna_candidato){
		this->nm_urna_candidato = nm_urna_candidato;
	}


	string Politico:: get_nm_partido(){
		return nm_partido;
	}
        void Politico:: set_nm_partido(string nm_partido){
		this->nm_partido = nm_partido;
	}
    
