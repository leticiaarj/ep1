#include<iostream>
#include"politico.hpp"
#include<string>
#include<fstream>
#include"votacao.hpp"
#include<cstdlib>
#include<vector>

using namespace std;

int main(){

	int numero_de_eleitores=0; 
	vector<string> nome_eleitor;
	string num_presi;
	string num_dep_df;
	string num_dep_br;
	string num_gov;
	string num_sen;
	int branco_nulo=0;
	string nome;
	Votacao *obj_votacao = new Votacao();


	cout << "Quantos eleitores irão votar?\n" <<endl;
	cin >> numero_de_eleitores; // Scanf, não precisa de ponteiro.	

	obj_votacao->ler_arquivo_BR();
	obj_votacao->ler_arquivo_DF();
	
	for(int i=0; i<numero_de_eleitores; i++){
		
		cout << "Nome eleitor: \n"<<endl;
		cin>>nome;
		nome_eleitor.push_back(nome);
		
		cout<< "Se deseja votar em branco digite, 1. Se deseja votar nulo digite, 2. Nenhuma das opções digite 0. \n";
		cin>>branco_nulo;
		
		if(branco_nulo==1){
		cout << "Voto em branco computado!\n";
		}
		else if(branco_nulo==2){
			cout << "Voto nulo computado!\n";
		}else{

			cout << "Digite o número do candidato a presidência\n"<<endl;
			cin >> num_presi;
			obj_votacao->achar_candidato(num_presi, "PRESIDENTE");
		
		}
		
		cout<< "Se deseja votar em branco digite, 1. Se deseja votar nulo digite, 2. Nenhuma das opções digite 0. \n";
                cin>>branco_nulo;
  
                if(branco_nulo==1){
                	cout << "Voto em branco computado!\n";
                }
                else if(branco_nulo==2){
                        cout << "Voto nulo computado!\n";
		}else{
			
			cout<< "Digite o número do canditado a Deputado Federal\n" << endl;
			cin>>num_dep_br;
			obj_votacao->achar_candidato(num_dep_br, "DEPUTADO FEDERAL");
		}

		cout<< "Se deseja votar em branco digite, 1. Se deseja votar nulo digite, 2. Nenhuma das opções digite 0. \n";
 		cin>>branco_nulo;

		if(branco_nulo==1){
			cout << "Voto em branco computado!\n";
		}
		else if(branco_nulo==2){
			cout << "Voto nulo computado!\n";

		}else{

		        cout<< "Digite o número do canditado a Deputado Distrital\n" << endl;
		        cin>> num_dep_df;
		        obj_votacao->achar_candidato(num_dep_df, "DEPUTADO DISTRITAL");
		}
		cout<< "Se deseja votar em branco digite, 1. Se deseja votar nulo digite, 2. Nenhuma das opções digite 0. \n";
                cin>>branco_nulo;
                 
                if(branco_nulo==1){
                        cout << "Voto em branco computado!\n";
                }
                else if(branco_nulo==2){
                        cout << "Voto nulo computado!\n";


		}else{

		        cout<< "Digite o número do canditado a governador\n" << endl;
		        cin>> num_gov;
		        obj_votacao->achar_candidato(num_gov, "GOVERNADOR");
		}
		cout<< "Se deseja votar em branco digite, 1. Se deseja votar nulo digite, 2. Nenhuma das opções digite 0. \n";
	        cin>>branco_nulo;

                if(branco_nulo==1){
                        cout << "Voto em branco computado!\n";
                }
                else if(branco_nulo==2){
                        cout << "Voto nulo computado!\n";

		}else{

		        cout<< "Digite o número do canditado a senador\n" << endl;
		        cin>> num_sen;
		        obj_votacao->achar_candidato(num_sen, "SENADOR");
		}
	}
	
}
